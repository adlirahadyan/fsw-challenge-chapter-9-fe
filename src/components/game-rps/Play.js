import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "../../style/game-rps/GameRps.css";
import Com from "./Com";
import Scoreboard from "./Scoreboard";
import Result from "./Result";
import Navbar from "../../components/NavbarElement";
import { Reset } from "./Reset";
import { Container, Row, Col } from "reactstrap";
import { Choice } from "./ChoiceComponent";
import Player from "./Player";

export default function Play() {
  let [game, setGame] = useState({
    myChoice: "",
    score: 0,
    pcChoice: "",
    gameResult: "",
  });

  const reset = () => {
    setGame({
      ...game,
      myChoice: "",
      score: 0,
      pcChoice: "",
      gameResult: "",
    });
  };
  const { score } = game;
  // const [gameResult, setgameResult] = useState("");

  const play = (e) => {
    const myChoice = e.target.dataset.id;

    const pcChoice = ["rock", "paper", "scissors"][
      Math.floor(Math.random() * 3)
    ];

    if (myChoice === "rock" && pcChoice === "scissors") {
      setGame({ ...(game.score += 1), ...(game.gameResult = "win") });
    } else if (myChoice === "rock" && pcChoice === "paper") {
      setGame({ ...(game.score -= 1), ...(game.gameResult = "lose") });
    } else if (myChoice === "scissors" && pcChoice === "paper") {
      setGame({ ...(game.score += 1), ...(game.gameResult = "win") });
    } else if (myChoice === "scissors" && pcChoice === "rock") {
      setGame({ ...(game.score -= 1), ...(game.gameResult = "lose") });
    } else if (myChoice === "paper" && pcChoice === "rock") {
      setGame({ ...(game.score += 1), ...(game.gameResult = "win") });
    } else if (myChoice === "paper" && pcChoice === "scissors") {
      setGame({ ...(game.score -= 1), ...(game.gameResult = "lose") });
    } else {
      setGame({ ...(game.gameResult = "draw") });
    }

    setGame({
      ...game,
      myChoice,
      pcChoice,
    });
  };

  return (
    <>
      <Navbar />
      <Container className="d-flex justify-content-center flex-column">
        <Row className="d-flex justify-content-center">
          <Col className="d-flex flex-column justify-content-center align-items-center">
            <div className="game">
              <div className="d-flex flex-column game player-choice align-items-center">
                <h3 className="text text-white center mb-3 mt-5">Player 1</h3>
                <Choice {...game} value="rock" onClick={play}></Choice>
                <Choice {...game} value="paper" onClick={play}></Choice>
                <Choice {...game} value="scissors" onClick={play}></Choice>
                <Player {...game}></Player>
              </div>
            </div>
          </Col>
          <Col className="d-flex flex-column justify-content-center align-items-center">
            <Result {...game}></Result>
          </Col>

          <Col className="d-flex justify-content-center flex-column align-items-center">
            <Com {...game} />
          </Col>
        </Row>
        <Row className="d-flex justify-content-center">
          <Reset onClick={reset} />
        </Row>
        <Row className="d-flex justify-content-center">
          <Scoreboard score={score} />
        </Row>
      </Container>
    </>
  );
}
