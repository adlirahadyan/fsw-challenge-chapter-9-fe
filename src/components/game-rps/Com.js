import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "../../style/game-rps/Game.css";

const Com = ({ myChoice, pcChoice, gameResult }) => {
  return (
    <>
      <div className="game">
        <div className="game pcChoice d-flex flex-column align-items-center">
          <h3 className="mb-3 mt-5 text text-white center">Computer</h3>
          {myChoice === "" ? (
            <>
              <h4 className="text-white">?</h4>
              <h4 className="text-white">Waiting Player's Choice</h4>
            </>
          ) : (
            <>
              <div
                className={`icon icon--${pcChoice} 
        }`}></div>
              <h4 className="text-white">PC's Choice: {pcChoice}</h4>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default Com;
