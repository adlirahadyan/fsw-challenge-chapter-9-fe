import "../../style/game-rps/Reset.css";
import { Button } from "reactstrap";

export const Reset = ({ onClick, myChoice }) => {
  return (
    myChoice !== "" && (
      <Button onClick={onClick} className="btn-warning">
        <h3 className="text-align-center">Reset</h3>
      </Button>
    )
  );
};
