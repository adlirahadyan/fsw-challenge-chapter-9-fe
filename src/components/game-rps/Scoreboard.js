import React from "react";
import "../../style/game-rps/Scoreboard.css";
import logo from "../../assets/game-rps/logo.png";
import { Container, Col, Row } from "reactstrap";

const Scoreboard = ({ score }) => {
  return (
    <Row className="d-flex board">
      <Col className="logo d-flex flex-column justify-content-center">
        <img src={logo} className="logo rps"></img>
      </Col>
      <Col className="score-board-text d-flex flex-column justify-content-center align-items-start">
        <div>Rock</div>
        <div>Paper</div>
        <div>Scissors</div>
      </Col>
      <Col className="score-box">
        <span>Score</span>
        <div className="score-box score">{score}</div>
      </Col>
    </Row>
  );
};

export default Scoreboard;
