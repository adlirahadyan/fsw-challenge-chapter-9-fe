import "../../style/game-rps/Choice.css";

export const Choice = ({ value, myChoice, onClick }) => {
  return (
    <div
      data-id={value}
      onClick={onClick}
      className={`p-5 icon icon--${value}`}></div>
  );
};
