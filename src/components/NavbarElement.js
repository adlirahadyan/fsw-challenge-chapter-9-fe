import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";
import "../style/Navbar.css";
import { Link } from "react-router-dom";
export default class Example extends React.Component {
  render() {
    return (
      <div className="p-2 d-flex justify-content-end">
        <Nav>
          <NavItem>
            <NavLink>
              <Link className="text-light h4" to={"/"}>
                Home
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link className="text-light h4" to={"/games/list"}>
                Game list
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link className="text-light h4" to={"/register"}>
                Register
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link className="text-light h4" to={"/login"}>
                Login
              </Link>
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    );
  }
}
