import React, { Component } from "react";
import "../style/Login.css";
import { useNavigate } from "react-router";
import Navbar from "../components/NavbarElement";

import {
  Container,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

export default class Login extends Component {
  state = {
    emailUsername: "",
    password: "",
  };
  login() {
    fetch("localhost:4000/api/v1/login", {
      method: "POST",
      body: JSON.stringify(this.state),
    }).then(console.log(this.state));
  }

  render() {
    return (
      <>
        <Navbar />
        <Container className="container-login">
          <Form className="login-form">
            <h2 className="title_login">LOGIN</h2>
            <FormGroup className="form-group">
              <Label for="email">Email or Username</Label>
              <Input
                id="Email"
                name="email"
                placeholder="Enter Your Email or Username"
                type="email"
                onChange={(e) =>
                  this.setState({ emailUsername: e.target.value })
                }
              />
            </FormGroup>{" "}
            <FormGroup className="form-group">
              <Label for="Password">Password</Label>
              <Input
                id="Password"
                name="password"
                placeholder="Enter Your Password"
                type="password"
                onChange={(e) => this.setState({ password: e.target.value })}
              />
            </FormGroup>{" "}
            {/* <div className="forgotPass">
                        <a href="#">Forgot Password?</a>
                    </div> */}
            <Nav className="forgotPass">
              <NavItem>
                <NavLink href="">Forgot Password?</NavLink>
              </NavItem>
            </Nav>
            <Button block color="success" onClick={() => this.login()}>
              Login
            </Button>
          </Form>
        </Container>
      </>
    );
  }
}
