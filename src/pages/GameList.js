import { Component } from "react";
import { Link } from "react-router-dom";
import React from "react";
import { Card, CardBody, CardTitle, Container } from "reactstrap";
import Navbar from "../components/NavbarElement";
import "../style/GameListPage.css";

class GameList extends Component {
  state = {
    gameList: [],
  };

  componentDidMount() {
    fetch("http://localhost:4000/api/v1/games/")
      .then((res) => res.json())
      .then((json) => {
        this.setState({ gameList: json.data });
      });
  }

  createElementGameList() {
    let elements = [];
    for (let i = 0; i < this.state.gameList.length; i++) {
      elements.push(
        <Card style={{ width: "18rem" }} className="m-3" key={i}>
          <img src={this.state.gameList[i].thumbnail_url} />
          <CardBody>
            <CardTitle tag="h5">
              <Link
                to={"/games/detail/" + this.state.gameList[i].id}
                className="removeHighlight">
                {this.state.gameList[i].name}
              </Link>
            </CardTitle>
          </CardBody>
        </Card>
      );
    }
    return elements;
  }

  render() {
    return (
      <>
        <Navbar />
        <Container className="containerGameList d-block ">
          <p className="d-flex justify-content-center h1 text-light">
            List of Available Games
          </p>
          <Container className="d-flex justify-content-center mt-5">
            {this.createElementGameList()}
          </Container>
        </Container>
      </>
    );
  }
}

export default GameList;
