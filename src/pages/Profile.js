import React, { Component } from "react";
import "../style/Profile.css";
// import { useNavigate } from "react-router";
// import { Link } from "react-router-dom";
import { FacebookLoginButton } from "react-social-login-buttons";

import {
  Container,
  Form,
  FormGroup,
  Button,
  Label,
  Input,
  Col,
  FormText,
} from "reactstrap";

export default class Profile extends Component {
  state = {
    name: "",
    email: "",
    username: "",
    password: "",
    address: "",
    description: "",
  };
  userId() {
    fetch("http://localhost:4000/api/v1/profile/:id", {
      method: "GET",
    }).then(console.log(this.state));
  }
  profile() {
    fetch("http://localhost:4000/api/v1/profile/:id", {
      method: "PUT",
      body: JSON.stringify(this.state),
    }).then(console.log(this.state));
  }
  render() {
    return (
      <Container className="container_profile">
        <Form className="register col-sm-7">
          <h2 className="title">PROFILE</h2>
          <FormGroup row>
            <Label for="Name" sm={2}>
              Name
            </Label>
            <Col sm={10}>
              <Input
                id="Name"
                name="name"
                placeholder="Write your name"
                type="text"
                onChange={(e) => this.setState({ name: e.target.value })}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="Email" sm={2}>
              Email
            </Label>
            <Col sm={10}>
              <Input
                id="Email"
                name="email"
                placeholder="Your Email"
                type="email"
                onChange={(e) => this.setState({ email: e.target.value })}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="Username" sm={2}>
              Username
            </Label>
            <Col sm={10}>
              <Input
                id="Username"
                name="username"
                placeholder="Write your username"
                type="text"
                onChange={(e) => this.setState({ username: e.target.value })}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="Password" sm={2}>
              Password
            </Label>
            <Col sm={10}>
              <Input
                id="Password"
                name="password"
                placeholder="Create your password"
                type="password"
                onChange={(e) => this.setState({ password: e.target.value })}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="Address" sm={2}>
              Address
            </Label>
            <Col sm={10}>
              <Input
                id="Address"
                name="address"
                placeholder="Write your address"
                type="text"
                onChange={(e) => this.setState({ address: e.target.value })}
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="Desc" sm={2}>
              Description
            </Label>
            <Col sm={10}>
              <Input
                id="Desc"
                name="desc"
                placeholder="Describe yourself here"
                type="textarea"
                onChange={(e) => this.setState({ description: e.target.value })}
              />
            </Col>
          </FormGroup>

          {/* <FormGroup>
                        <Label for="FileUpload">
                        Upload Foto Profile
                        </Label>
                        <Input
                        id="FileUpload"
                        name="file"
                        type="file"
                        />
                        <FormText color="white">
                        File maksimal berukuran 3mb
                        </FormText>
                    </FormGroup> */}

          <FormGroup className="button-profile">
            <Button
              block
              type="submit"
              color="success"
              onClick={() => this.profile()}>
              Update
            </Button>
          </FormGroup>
          {/* <div className='text-center pt-2'>
                        Let's connect with your social account!
                    </div>
                    <FacebookLoginButton className='mt-2 mb-4'/> */}
        </Form>
      </Container>
    );
  }
}
