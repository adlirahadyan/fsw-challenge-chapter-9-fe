import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./pages/Login";
import Register from "./pages/register";
import LandingPage from "./pages/LandingPage";
import Profile from "./pages/Profile";
import Play from "./components/game-rps/Play";
import GameList from './pages/GameList';
import GameDetails from './pages/GameDetails';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/profile" element={<Profile />} />
        <Route path="/login" element={<Login />} />
        <Route path="/" element={<LandingPage />} />
        <Route path="/register" element={<Register />} />
        <Route path="/games/play/1" element={<Play />}></Route>
        <Route path='/games/list' element={<GameList/>}></Route>
        <Route path='/games/detail/:gameId' element={<GameDetails/>}></Route>
      </Routes>
    </Router>
  );
}

export default App;
